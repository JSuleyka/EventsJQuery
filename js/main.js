$(document).ready(function() {

    //Establecer el valor del elemento input igual al valor del elemento label
    $(".inputText").on("keypress", function(e) {
    	var inputval = $("input").val();
    	if (e.keyCode==13)        
    	{
    		console.log(inputval);
     		$("#label").html(inputval);
     	}
    });

    //Añadir la clase "hint" al elemento input
    $(".btnAddClass").on("click", function(){
		$(".inputClass input").addClass("currentInput");
	});

    //Remover el elemento label
	$(".btnRemove").on("click", function(){
		$(".removeLabel label").remove();
	});

    //Vincular un evento focus en el input para remover el texto de sugerencia y la clase "hint"
    var value =$(".focus input").val();
    console.log(value);
	$(".focus input").on({
        "focusin" : function() {          
        	$(this).removeClass("hint");
        	$(this).val("");
        },

       "focusout" : function() {
            console.log(value);
            $(this).val(value);
        }
    });

    //Ocultar todos los elementos div.module;
    $("#btnModule").on("click",function() {
        $("div.module").hide();
    });

    //Crear una lista desordenada antes del primer div.module para utilizar como pestañas
    //Interactuar con cada div utilizando $.fn.each. Por cada uno, utilizar el texto del elemento h2 como el texto para el ítem de la lista desordenada
    //Vincular un evento click a cada ítem de la lista de forma que:
   
    $('#tabs div').css('position', 'absolute').not(':first').hide();    //Muestre el div correspondiente y oculte el otro;
    
    $('#tabs a').on("click", function(){                      
        $('#tabs a').removeClass('current');        //Remueva la clase "current" del otro ítem de la lista;
        $(this).addClass('current');                //Añada la clase "current" al ítem seleccionado;

        $('#tabs div').fadeOut(350).filter(this.hash).fadeIn(350);      //Finalmente, mostrar la primera pestaña.
        return false;
    });

});